/** @format */

console.log("panggil satu function yang kalian buat di folder perkenalan");
// panggil function di bawah ket(untuk line ganjil laki-laki, line genap perempuan)
const okto = require("./perkenalan/okto.js");
console.log(okto());

const perkenalanLuthfiyah = require("./perkenalan/luthfiyah.js");
const perkenalanJarot = require("./perkenalan/jarot");
const alif = require("./perkenalan/alif");
const perkenalanFauzanAjip = require("./perkenalan/perkenalanFauzanAjip");
const perkenalanNaufal = require("./perkenalan/naufal");
const file = require("./perkenalan/rinaldy");
const perkenalanAdelia = require("./perkenalan/adelia.js");
const ari = require("./perkenalan/ari");
const imam = require("./perkenalan/imam");
const perkenalanKurniawan = require("./perkenalan/kurniawan");
const triv = require("./perkenalan/trivita");
const zaky = require("./perkenalan/zaky");
const perkenalanShandy = require("./perkenalan/shandy");
const ahyar = require("./perkenalan/ahyar");
const perkenalanAndreas = require("./perkenalan/andreas.js");
const anang = require("./perkenalan/anang");
const perkenalanDiah = require("./perkenalan/diah");

console.log(perkenalanLuthfiyah());
console.log(perkenalanJarot());
console.log(alif.perkenalan_alif());
console.log(perkenalanFauzanAjip());
console.log(perkenalanNaufal());
console.log(file.perkenalanRinaldy());
console.log(perkenalanAdelia());
console.log(ari.perkenalan());
console.log(imam());
console.log(perkenalanKurniawan());
console.log(zaky);
console.log(triv.perkenalanTrivita());
console.log(require(__dirname + "/./perkenalan/raka.js")());
console.log(perkenalanShandy());
console.log(ahyar);
console.log(perkenalanAndreas());
console.log(anang());
console.log(perkenalanDiah());