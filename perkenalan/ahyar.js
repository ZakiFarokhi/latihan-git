function perkenalanAhyar() {
  let nama = "Ahmad Taqiudin Ahyari";
  let tglLahir = "18/12/2000";
  let kampus = "Universitas Muhammdiyah Malang";
  let jurusan = "Informatika";

  return {
    nama: nama,
    tanggal_lahir: tglLahir,
    kampus: kampus,
    jurusan: jurusan,
  };
}

module.exports = perkenalanAhyar();
