/** @format */

function perkenalanContoh() {
  let nama = 'Maulana Imam Malik';
  let tanggalLahir = '18/08/2001';
  let kampus = 'Universitas Komputer Indonesia';
  let jurusan = 'Teknik Informatika';

  return { nama: nama, tanggal_lahir: tanggalLahir, kampus: kampus, jurusan: jurusan };
}

module.exports = perkenalanContoh;
