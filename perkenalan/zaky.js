function perkenalanZaky() {
  let nama = "Muhammad Zaky Aonillah";
  let tglLahir = "10/11/1999";
  let kampus = "Telkom University";
  let jurusan = "S1 Informatika";

  return { nama: nama, tanggal_lahir: tglLahir, kampus: kampus, jurusan: jurusan };
}

module.exports = perkenalanZaky();
