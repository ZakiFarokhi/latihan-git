function perkenalanJarot(){
    const nama = "Jarot Setiawan";
    const tglLahir = "25-03-2001";
    const kampus = "Universitas Muhammadiyah Surakarta";
    const jurusan = "Teknik Informatika";

    return {nama:nama, tgl_lahir:tglLahir, kampus:kampus, jurusan:jurusan};
}

module.exports = perkenalanJarot;