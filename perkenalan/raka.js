module.exports = function (){
    const nama = "Muhammad Rakha Firjatullah",
        tanggalLahir = "04/09/01",
        kampus = "Universitas Pembangunan Nasional Veteran Jawa Timur",
        jurusan = "Informatika"

    return {
        nama, 
        tanggalLahir, 
        kampus, 
        jurusan
    }
}