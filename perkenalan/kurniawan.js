const perkenalanKurniawan = () => {
  let nama = "Kurniawan Cristianto";
  let tanggalLahir = "17/04/2000";
  let kampus = "Universitas Sriwijaya";
  let jurusan = "Teknik Informatika";

  return { nama, tanggal_lahir: tanggalLahir, kampus, jurusan };
};

module.exports = perkenalanKurniawan;
